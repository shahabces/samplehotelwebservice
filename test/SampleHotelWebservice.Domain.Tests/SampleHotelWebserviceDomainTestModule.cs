﻿using SampleHotelWebservice.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace SampleHotelWebservice
{
    [DependsOn(
        typeof(SampleHotelWebserviceEntityFrameworkCoreTestModule)
        )]
    public class SampleHotelWebserviceDomainTestModule : AbpModule
    {

    }
}