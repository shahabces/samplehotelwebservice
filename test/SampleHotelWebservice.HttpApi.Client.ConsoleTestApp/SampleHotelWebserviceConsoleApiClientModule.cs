﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace SampleHotelWebservice.HttpApi.Client.ConsoleTestApp
{
    [DependsOn(
        typeof(SampleHotelWebserviceHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class SampleHotelWebserviceConsoleApiClientModule : AbpModule
    {
        
    }
}
