﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace SampleHotelWebservice.Pages
{
    public class Index_Tests : SampleHotelWebserviceWebTestBase
    {
        [Fact]
        public async Task Welcome_Page()
        {
            var response = await GetResponseAsStringAsync("/");
            response.ShouldNotBeNull();
        }
    }
}
