﻿using Volo.Abp;

namespace SampleHotelWebservice.EntityFrameworkCore
{
    public abstract class SampleHotelWebserviceEntityFrameworkCoreTestBase : SampleHotelWebserviceTestBase<SampleHotelWebserviceEntityFrameworkCoreTestModule> 
    {

    }
}
