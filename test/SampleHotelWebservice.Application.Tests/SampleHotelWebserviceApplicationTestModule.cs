﻿using Volo.Abp.Modularity;

namespace SampleHotelWebservice
{
    [DependsOn(
        typeof(SampleHotelWebserviceApplicationModule),
        typeof(SampleHotelWebserviceDomainTestModule)
        )]
    public class SampleHotelWebserviceApplicationTestModule : AbpModule
    {

    }
}