﻿using Volo.Abp.Localization;

namespace SampleHotelWebservice.Localization
{
    [LocalizationResourceName("SampleHotelWebservice")]
    public class SampleHotelWebserviceResource
    {

    }
}