﻿using SampleHotelWebservice.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace SampleHotelWebservice.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(SampleHotelWebserviceEntityFrameworkCoreDbMigrationsModule),
        typeof(SampleHotelWebserviceApplicationContractsModule)
        )]
    public class SampleHotelWebserviceDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
