﻿using SampleHotelWebservice.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace SampleHotelWebservice.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class SampleHotelWebserviceController : AbpController
    {
        protected SampleHotelWebserviceController()
        {
            LocalizationResource = typeof(SampleHotelWebserviceResource);
        }
    }
}