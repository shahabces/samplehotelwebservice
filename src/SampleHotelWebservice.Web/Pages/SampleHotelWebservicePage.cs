﻿using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using SampleHotelWebservice.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace SampleHotelWebservice.Web.Pages
{
    /* Inherit your UI Pages from this class. To do that, add this line to your Pages (.cshtml files under the Page folder):
     * @inherits SampleHotelWebservice.Web.Pages.SampleHotelWebservicePage
     */
    public abstract class SampleHotelWebservicePage : AbpPage
    {
        [RazorInject]
        public IHtmlLocalizer<SampleHotelWebserviceResource> L { get; set; }
    }
}
