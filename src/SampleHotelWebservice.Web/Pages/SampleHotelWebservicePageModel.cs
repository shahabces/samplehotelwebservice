﻿using SampleHotelWebservice.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace SampleHotelWebservice.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class SampleHotelWebservicePageModel : AbpPageModel
    {
        protected SampleHotelWebservicePageModel()
        {
            LocalizationResourceType = typeof(SampleHotelWebserviceResource);
        }
    }
}