﻿using AutoMapper;

namespace SampleHotelWebservice.Web
{
    public class SampleHotelWebserviceWebAutoMapperProfile : Profile
    {
        public SampleHotelWebserviceWebAutoMapperProfile()
        {
            //Define your AutoMapper configuration here for the Web project.
        }
    }
}
