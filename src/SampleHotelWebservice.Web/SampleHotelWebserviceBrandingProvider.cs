﻿using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Components;
using Volo.Abp.DependencyInjection;

namespace SampleHotelWebservice.Web
{
    [Dependency(ReplaceServices = true)]
    public class SampleHotelWebserviceBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "SampleHotelWebservice";
    }
}
