﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace SampleHotelWebservice.Data
{
    /* This is used if database provider does't define
     * ISampleHotelWebserviceDbSchemaMigrator implementation.
     */
    public class NullSampleHotelWebserviceDbSchemaMigrator : ISampleHotelWebserviceDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}