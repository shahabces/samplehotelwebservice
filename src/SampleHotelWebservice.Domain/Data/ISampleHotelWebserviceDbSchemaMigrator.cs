﻿using System.Threading.Tasks;

namespace SampleHotelWebservice.Data
{
    public interface ISampleHotelWebserviceDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
