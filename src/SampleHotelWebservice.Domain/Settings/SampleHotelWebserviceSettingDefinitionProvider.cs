﻿using Volo.Abp.Settings;

namespace SampleHotelWebservice.Settings
{
    public class SampleHotelWebserviceSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(SampleHotelWebserviceSettings.MySetting1));
        }
    }
}
