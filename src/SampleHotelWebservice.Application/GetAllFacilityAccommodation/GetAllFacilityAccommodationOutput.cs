﻿namespace SampleHotelWebservice
{
    public class GetAllFacilityAccommodationOutput
    {
        public int Id { get; set; }
        public int FacilityId { get; set; }
        public int AccommodationId { get; set; }
    }
}