﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleHotelWebservice.GetAllLocationCategory
{
    public class GetAllLocationCategoryOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
