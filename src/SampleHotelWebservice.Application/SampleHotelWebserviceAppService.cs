﻿using System;
using System.Collections.Generic;
using System.Text;
using SampleHotelWebservice.Localization;
using Volo.Abp.Application.Services;

namespace SampleHotelWebservice
{
    /* Inherit your application services from this class.
     */
    public abstract class SampleHotelWebserviceAppService : ApplicationService
    {
        protected SampleHotelWebserviceAppService()
        {
            LocalizationResource = typeof(SampleHotelWebserviceResource);
        }
    }
}
