﻿using System;

namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class GetAllHotelsOutput
        {
            public int Id { get; set; }
            public string AccommodationName { get; set; }
            public StarsEnum Stars { get; set; }
            public DateTime HotelCreationDate { get; set; }
            public byte NumberOfRoom { get; set; }
            public byte LobbyCapacity { get; set; }
            public byte NumberOfFloors { get; set; }
            public GeoLocation GeoLocation { get; set; }
            public byte AllUserRate { get; set; }
            public AccommodationType AccommodationType { get; set; }
            public DateTime RoomEnterTime { get; set; }
            public DateTime RoomExitTime { get; set; }
            public Location Location { get; set; }
            public byte NumberOfRoomAvailableForResevation { get; set; }
            public decimal StartPriceFrom { get; set; }

        }
    }
}
