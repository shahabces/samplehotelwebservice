﻿namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class GetAllHotelRuleOutput
        {
            public int HotelId { get; set; }
            public int RuleId { get; set; }
        }
    }
}
