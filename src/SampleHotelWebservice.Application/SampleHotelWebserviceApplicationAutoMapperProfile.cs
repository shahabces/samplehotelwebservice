﻿using AutoMapper;

namespace SampleHotelWebservice
{
    public class SampleHotelWebserviceApplicationAutoMapperProfile : Profile
    {
        public SampleHotelWebserviceApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
