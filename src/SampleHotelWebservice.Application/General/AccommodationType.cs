﻿namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class AccommodationType
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
