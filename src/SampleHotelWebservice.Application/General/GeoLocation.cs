﻿namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class GeoLocation
        {
            public double Lat { get; set; }
            public double Lon { get; set; }
        }
    }
}
