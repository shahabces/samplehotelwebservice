﻿namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class Region
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
