﻿namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class Country
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
