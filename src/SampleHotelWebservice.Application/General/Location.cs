﻿namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class Location
        {
            public Country Country { get; set; }
            public City City { get; set; }
            public Region Region { get; set; }
        }
    }
}
