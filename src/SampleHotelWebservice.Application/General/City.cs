﻿namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class City
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
