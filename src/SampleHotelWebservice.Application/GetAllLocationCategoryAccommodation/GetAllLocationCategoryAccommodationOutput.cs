﻿namespace SampleHotelWebservice
{
    public class GetAllLocationCategoryAccommodationOutput
    {
        public int LocationCategoryId { get; set; }
        public int AccommodationId { get; set; }
        public string Explenation { get; set; }
    }
}