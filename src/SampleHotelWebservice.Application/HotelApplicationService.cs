﻿using SampleHotelWebservice.GetAllLocationCategory;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Services;

namespace SampleHotelWebservice
{
    public partial class HotelApplicationService : ApplicationService
    {
        public List<GetAllHotelRuleOutput> GetAllHotelRule()
        {
            return new List<GetAllHotelRuleOutput>();
        }
        public List<GetAllRulesOutput> GetAllRules()
        {
            return new List<GetAllRulesOutput>();
        }
        public List<GetAllHotelsOutput> GetAllHotels(GetAllHotelsInput getAllHotelInput)
        {
            return new List<GetAllHotelsOutput>();
        }

        public List<GetAllLocationCategoryOutput> GetAllLocationCategory()
        {
            return new List<GetAllLocationCategoryOutput>();
        }

        public List<GetAllLocationCategoryAccommodationOutput> GetAllLocationCategoryAccommodation()
        {
            return new List<GetAllLocationCategoryAccommodationOutput>();
        }

        public List<GetAllFacilitiesOutput> GetAllFacilities()
        {
            return new List<GetAllFacilitiesOutput>();
        }

        public List<GetAllFacilityAccommodationOutput> GetAllFacilityAccommodation()
        {
            return new List<GetAllFacilityAccommodationOutput>();
        }

        public List<GetAllMarkettingTagsOutput> GetAllMarkettingTags()
        {
            return new List<GetAllMarkettingTagsOutput>();
        }
    }
}
