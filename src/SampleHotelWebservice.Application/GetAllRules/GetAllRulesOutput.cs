﻿namespace SampleHotelWebservice
{
    public partial class HotelApplicationService
    {
        public class GetAllRulesOutput
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
