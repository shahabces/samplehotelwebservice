﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SampleHotelWebservice.Data;
using Volo.Abp.DependencyInjection;

namespace SampleHotelWebservice.EntityFrameworkCore
{
    public class EntityFrameworkCoreSampleHotelWebserviceDbSchemaMigrator
        : ISampleHotelWebserviceDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreSampleHotelWebserviceDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the SampleHotelWebserviceMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<SampleHotelWebserviceMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}