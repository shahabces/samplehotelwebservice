﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace SampleHotelWebservice.EntityFrameworkCore
{
    [DependsOn(
        typeof(SampleHotelWebserviceEntityFrameworkCoreModule)
        )]
    public class SampleHotelWebserviceEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<SampleHotelWebserviceMigrationsDbContext>();
        }
    }
}
