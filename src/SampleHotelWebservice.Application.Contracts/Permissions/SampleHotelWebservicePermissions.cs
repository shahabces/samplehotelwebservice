﻿namespace SampleHotelWebservice.Permissions
{
    public static class SampleHotelWebservicePermissions
    {
        public const string GroupName = "SampleHotelWebservice";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}