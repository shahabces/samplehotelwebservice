﻿using SampleHotelWebservice.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace SampleHotelWebservice.Permissions
{
    public class SampleHotelWebservicePermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(SampleHotelWebservicePermissions.GroupName);

            //Define your own permissions here. Example:
            //myGroup.AddPermission(SampleHotelWebservicePermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<SampleHotelWebserviceResource>(name);
        }
    }
}
